import operator


data_set = {"Kostas": ["java", "prolog", "ai"],
            "Petros": ["c", "c++", "ml",],
            "Stathis": ["c++", "java", "php", "da"],
            "Giorgos": ["python", "php", "da"],
            "Arianna": ["c++", "python", "da"],
            "Lydia": ["ai", "ml", "da"],
            "Alexia": ["c", "python", "ai"],
}

# data_set = {"Kostas": ["java", "prolog", "ai"],
#             "Arianna": ["c++", "python", "da"],
#             "Lydia": ["ai", "ml", "da"],
#             "Georgia": ["c", "java", "python", "ai"],
#             "Petros": ["c", "c++", "ml"],
#             "Stathis": ["c++", "java", "php", "da"],
#             "Giorgos": ["python", "php", "da"],
#             "maria": ["c++", "python", "java", "da"],
#             "Akis": ["ai", "ml", "da"],
#             "Alexia": ["c", "python", "ai"],
#             "Pavlos": ["c", "c++", "ml", "prolog"],
#             "Tom": ["c++", "java", "php", "da"],
#             "Giota": ["python", "php", "da"],
#             "Nika": ["c++", "c", "python", "da"],
#             "Marika": ["ai", "ml", "da"],
#             "Nikolaos": ["c", "python", "ai", "da"],
#             "Nikos": ["c", "c++", "python", "ml"],
#             "Giannis": ["c++", "java", "ai", "php", "da"],
#             "Panos": ["python", "php", "da"],
# }

# data_set = {"Kostas": ["java", "prolog", "ai"],
#             "Arianna": ["c++", "python", "da"],
#             "Lydia": ["ai", "ml", "da"],
#             "Alexia": ["c", "python", "ai"],
#             "Petros": ["c", "c++", "ml"],
#             "Stathis": ["c++", "java", "php", "da"],
#             "Giorgos": ["python", "php", "da"],
# }

languages = ["c", "c++", "python", "java", "prolog", "php", "ai", "ml", "da"]


def initialize_tree():
    # For every language which only one person can write, we add this person to a list (root )
    # If there is not any language then we return an empty list
    un_man = []
    count_man = 0
    for lang in languages:
        for person in data_set:
            if lang in data_set[person]:
                count_man +=1
                last_man = person
        if count_man == 1:
            un_man.append(last_man)
        count_man = 0
    return un_man


def heuristic_function():
    person_value = {}
    lang_freq = languages_freq()
    additional_weight = 0
    for person in data_set:
        for lang in data_set[person]:
            additional_weight += len(data_set)/lang_freq[lang]
        person_value[person] = len(data_set[person]) + additional_weight
        additional_weight = 0
    return person_value


def possible_selection(solution, heuristic_results):
    # return a descending list of remaining (unselected) people
    # [{'name': 'Stathis', 'weight': 15.083333333333334},{'name': 'Petros', 'weight': 12.333333333333334},
    #  {'name': 'Alexia', 'weight': 11.166666666666668}, {'name': 'Giorgos', 'weight': 10.583333333333334},
    #  {'name': 'Lydia', 'weight': 10.583333333333334}, {'name': 'Arianna', 'weight': 9.416666666666668}]
    rest_persons = []
    for person in data_set:
        if person not in solution:
            rest_persons.append({'name': person, 'weight': heuristic_results[person]})
    rest_persons.sort(reverse=True, key=operator.itemgetter('weight'))
    return rest_persons


def check_for_solution(solution):
    # Check if our list solution, which contains the already added people, is the solution of our problem
    known_lang = []
    for person in solution:
        known_lang = known_lang + data_set[person]
    known_lang = list(set(known_lang))
    if len(known_lang) == 9:
        return True
    else:
        return False


def find_solution(solution, heuristic_results):
    # Every time we call this function we check if we found a solution. If not, we take the person with the highest
    # heuristic value, add it to the solution list and then call its self again .
    if check_for_solution(solution):
        print("The best solution is ", solution)
        return -1
    else:
        remain_person = possible_selection(solution, heuristic_results)
        for item in remain_person:
            solution.append(item['name'])
            flag = find_solution(solution, heuristic_results)
            if flag == -1:
                return -1
        return "no solution"


def languages_freq():
    # Calculate and return how many people know each language
    # e.g. {'c': 2, 'c++': 3, 'python': 3, 'java': 2, 'prolog': 1, 'php': 2, 'ai': 3, 'ml': 2, 'da': 4}
    counter = 0
    lang_freq = {}
    for lang in languages:
        for person in data_set:
            if lang in data_set[person]:
                counter += 1
        lang_freq[lang] = counter
        counter = 0
    return lang_freq


if __name__ == "__main__":
    root = initialize_tree()
    solution = root
    heuristic_results = heuristic_function()
    result = find_solution(solution, heuristic_results)
    if result == "no solution":
        print("We did not find any solution")
